#pragma once

#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define BAUD 19600
#define MYUBRR (F_CPU/16/BAUD-1)
#define LIMITER '\0'

//Could be useless
extern uint8_t * cmd_buffer;

void UART_init(void);
void UART_putChar(uint8_t c);
uint8_t UART_getChar(void);
uint8_t UART_getString(uint8_t* buf);
void UART_putString(uint8_t* buf);


uint8_t UART_getVarString(uint8_t* buf, uint8_t start_pos, uint8_t size);
uint8_t UART_putVarString(uint8_t* buf, uint8_t start_pos, uint8_t size);
uint8_t UART_handle_command(uint8_t* buf);
uint8_t write_Char(uint8_t c);
uint8_t read_Char(void);
uint8_t getReadSpace(void);
uint8_t getWriteSpace(void);

#define BUF_SIZE 128
#define CMD_BUF_SIZE 16
extern uint8_t read_buf_pos; //read buffer (where the PC reads)
extern uint8_t write_buf_pos; //write buffer (where the PC writes)
extern uint8_t * read_buffer; //points to ReadBuffer[BUF_SIZE]
extern uint8_t * write_buffer; //points to WriteBuffer[BUF_SIZE]


extern uint8_t pc_read_pos;
extern uint8_t pc_write_pos;

extern uint8_t avr_read_pos;
extern uint8_t avr_write_pos;

#define READ_CHAR 'r'
#define WRITE_CHAR 'w'
#define START_CHAR 'g'
#define STOP_CHAR 's'
#define DELIMITER '\r'
#define DEFAULT_ERR (uint8_t *) "ed\r"
#define NOT_SUPPORTED_ERR (uint8_t *) "en\r"
#define NOT_SUPPORTED_DESCHEDULE_ERR (uint8_t *) "Error! Thread Status not recognised!"
