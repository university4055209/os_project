#include "utils.h"
#include <stdio.h>
#include <stdint.h>
#include <uart.h>
#include <util/atomic.h>

void int_to_char(uint8_t * buf, uint8_t num){
    //Since num is uint8_t, we'll only need 3 chars
    //Numbers start on the 30th position in the ascii table
    uint8_t u;
    uint8_t da;
    uint8_t h;
    u = num%10;
    da= ((num%100)-u)/10;
    h = (num-((da*10)+u))/100;
    uint8_t index = 0;
    if(h){
        index = 4;
        buf[index--] = 0;
        buf[index--] = DELIMITER;
        buf[index--] = u+48;
        buf[index--] = da+48;
        buf[index] = h+48;
    }
    else if(da){
        index = 3;
        buf[index--] = 0;
        buf[index--] = DELIMITER;
        buf[index--] = u+48;
        buf[index] = da+48;
    }
    else{
        index = 2;
        buf[index--] = 0;
        buf[index--] = DELIMITER;
        buf[index] = u+48;
    }
    return;
}

//micro strcmp. Returns 0 if equal, 1 if s1>s2, 2 if s2>s1
uint8_t u_strcmp(uint8_t * s1, uint8_t * s2){
    uint8_t * sc1 = s1;
    uint8_t * sc2 = s2;
    while(*sc1 && *sc2){
        if(*sc1 == *sc2){
            sc1++; sc2++;
            continue;
        }
        else if(*sc1 > *sc2){
            return 1;
        }
        else return 2;
    }
    if(*sc1){
        return 1;
    }
    else if(*sc2){
        return 2;
    }
    else return 0;
}

uint8_t u_atoi(uint8_t * s){
    uint8_t result = 0;
    uint8_t lenght = 0;
    uint8_t * buf = s;
    while(*buf && *buf>=48 && *buf<=57){
        lenght++;
        buf++;
    }
    if(lenght==0||lenght>3){
        //We cannot have numbers larger than 255
        return 0;
    }
    uint8_t power = 1;
    while(lenght--){
        result += (s[lenght]-48)*power;
        power *= 10;
    }
    if(result>255){
        //same as above
        return 0;
    }
    return result;
}

/*
int main(){
    int_to_char(NULL, 230);
    int_to_char(NULL, 50);
}
*/
