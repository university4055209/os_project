#include <avr/interrupt.h>
#include <avr/io.h>
#include <assert.h>
#include "tcb.h"
#include "tcb_list.h"
#include "atomport_asm.h"
#include "timer.h"
#include <stdint.h>
#include <uart.h>
#include <util/atomic.h>
#include <utils.h>
//Why was this not included?
#include "utils.h"
#include "scheduler.h"
// the (detached) running process
TCB* current_tcb=NULL;

// the running queue
TCBList running_queue={
  .first=NULL,
  .last=NULL,
  .size=0
};

TCBList waiting_read_queue={
  .first=NULL,
  .last=NULL,
  .size=0
};

TCBList waiting_write_queue={
  .first=NULL,
  .last=NULL,
  .size=0
};

void deSchedule(ThreadStatus newStatus){
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCBList * target_list;
    switch (newStatus)
    {
      case WaitingRead:
      {
        target_list = &waiting_read_queue;
        current_tcb->status = WaitingRead;
      }break;
      case WaitingWrite:
      {
        target_list = &waiting_write_queue;
        current_tcb->status = WaitingWrite;
      }break;
      default:
      {
        UART_putString(NOT_SUPPORTED_DESCHEDULE_ERR);
      }return;
    }
    TCBList_enqueue(target_list, current_tcb);
    reSchedule();
  }
}

//reschedule is called by schedule or deschedule. deschedule has the call contained inside
//an ATOMIC_BLOCK whereas schedule is called by an ISR, which makes it by definition immune
//to new interrupts. For this reason we do not need another ATOMIC_BLOCK
void reSchedule(void){
  TCB* old_tcb = current_tcb;
  current_tcb = NULL;
  //No queueing happens since that's been done by the caller
  if(global_flags.check_read_first){
    if(waiting_read_queue.size && !global_flags.avr_read_lock){
      //If there's a process waiting and the lock is not set
      UART_putString((uint8_t *)"\n=======We have a reading thread waiting to be woken up=======\n");
      current_tcb = TCBList_dequeue(&waiting_read_queue);
    }
    else if(waiting_write_queue.size && !global_flags.avr_write_lock){
      //Same as above
      UART_putString((uint8_t *)"\n=======We have a writing thread waiting to be woken up=======\n");
      current_tcb = TCBList_dequeue(&waiting_write_queue);
    }
    global_flags.check_read_first = 0;
  }
  else{
    //We just reverse the order
    if(waiting_write_queue.size && !global_flags.avr_write_lock){
      //Same as above
      current_tcb = TCBList_dequeue(&waiting_write_queue);
    }
    else if(waiting_read_queue.size && !global_flags.avr_read_lock){
      //If there's a process waiting and the lock is not set
      current_tcb = TCBList_dequeue(&waiting_read_queue);
    }
    global_flags.check_read_first = 1;
  }
  if(current_tcb == NULL){
    //There's no one waiting. We poll on the running queue
    //We do not need any check since the idle thread is always ready tu run
    current_tcb = TCBList_dequeue(&running_queue);
  }
  if(old_tcb != current_tcb){
    archContextSwitch(old_tcb, current_tcb);
  }
  return;
}

void startSchedule(void){
  cli();
  current_tcb=TCBList_dequeue(&running_queue);
  assert(current_tcb);
  timerStart();
  archFirstThreadRestore(current_tcb);
}

void schedule(void) {
  //TCB* old_tcb=current_tcb;
  // we put back the current thread in the queue
  TCBList_enqueue(&running_queue, current_tcb);

  reSchedule();
  // we fetch the next;
  //current_tcb=TCBList_dequeue(&running_queue);
  // we jump to it (useless if it is the only process)
  //if (old_tcb!=current_tcb)
    //archContextSwitch(old_tcb, current_tcb);
}
