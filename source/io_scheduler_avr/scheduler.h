#include "tcb.h"
#include "tcb_list.h"

// the (detached) running process
extern TCB* current_tcb;

// the running queue
extern TCBList running_queue;
extern TCBList waiting_read_queue;
extern TCBList waiting_write_queue;

void startSchedule(void);

void deSchedule(ThreadStatus newStatus);
void reSchedule(void);
// used in the ISR of the timer
void schedule(void);
     
