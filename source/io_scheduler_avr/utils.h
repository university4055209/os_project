#pragma once

#include <stdint.h>


void int_to_char(uint8_t * buf, uint8_t num);
//micro strcmp. Returns 0 if equal, 1 if s1>s2, 2 if s2>s1
uint8_t u_strcmp(uint8_t * s1, uint8_t * s2);
//micro atoi. Return the number pointed by s given that the number is lower than 256
uint8_t u_atoi(uint8_t *s);

#define DELIMITER '\r'

typedef struct globlFlags{
    uint8_t should_continue:1;
    //PC has something to read
    uint8_t can_read:1;
    //PC has something to write
    uint8_t can_write:1;
    //Scheduler priority over read/write queue
    uint8_t check_read_first:1;
    //AVR can read (unlocks read_queue)
    uint8_t avr_read_lock:1;
    //AVR can write (unlocks write_queue)
    uint8_t avr_write_lock:1;
}globlFlags;

extern globlFlags global_flags;
