#include <avr/interrupt.h>
#include <avr/io.h>
#include <assert.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdio.h>
#include "tcb.h"
#include "tcb_list.h"
#include "uart.h"
#include "atomport_asm.h"
#include "scheduler.h"
#include "utils.h"
#define THREAD_STACK_SIZE 256
#define IDLE_STACK_SIZE 128


//IO buffers (Extern)
uint8_t * read_buffer; //points to ReadBuffer[BUF_SIZE]
uint8_t * write_buffer; //points to WriteBuffer[BUF_SIZE]
uint8_t * cmd_buffer; //Used by uart_handler


uint8_t pc_read_pos; //pc read position (ReadBuffer)
uint8_t pc_write_pos; //pc write position (WriteBuffer)
uint8_t avr_read_pos; //avr read position (WriteBuffer)
uint8_t avr_write_pos; //avr write position (ReadBuffer)

globlFlags global_flags;
//statically allocated variables where we put our stuff

TCB idle_tcb;
uint8_t idle_stack[IDLE_STACK_SIZE];

void idle_fn(uint32_t thread_arg __attribute__((unused))){
  while(1) {
    cli();
    //printf("i\n");
    UART_putString((uint8_t *)"i\n");
    sei();
    _delay_ms(100);
  }
}

TCB p1_tcb;
uint8_t p1_stack[THREAD_STACK_SIZE];
void p1_fn(uint32_t arg __attribute__((unused))){
  uint8_t wheel = 0;
  while(1){
    cli();
    //printf("p1\n");
    if(!write_Char(48+(wheel%10))){
      UART_putString((uint8_t *)"p1: wrote!\n");
      wheel++;
    }
    else{
      UART_putString((uint8_t *)"p1 going to sleep...\n");
      deSchedule(WaitingWrite);
    }
    sei();
    _delay_ms(100);
  }
}

TCB p2_tcb;
uint8_t p2_stack[THREAD_STACK_SIZE];
void p2_fn(uint32_t arg __attribute__((unused))){
  uint8_t char_read;
  while(1){
    cli();
    //printf("p2\n");
    char_read = read_Char();
    if(char_read!=255){
      UART_putString((uint8_t *)"p2: read: ");
      UART_putChar(char_read);
      UART_putChar('\n');
    }
    else{
      UART_putString((uint8_t *)"p2 is going to sleep...\n");
      deSchedule(WaitingRead);
    }
    sei();
    _delay_ms(100);
  }
}



int main(void){
  // we need //printf for debugging
  //printf_init();

  TCB_create(&idle_tcb,
             idle_stack+IDLE_STACK_SIZE-1,
             idle_fn,
             0);

  TCB_create(&p1_tcb,
             p1_stack+THREAD_STACK_SIZE-1,
             p1_fn,
             0);

  TCB_create(&p2_tcb,
             p2_stack+THREAD_STACK_SIZE-1,
             p2_fn,
             0);

  
  TCBList_enqueue(&running_queue, &p1_tcb);
  TCBList_enqueue(&running_queue, &p2_tcb);
  TCBList_enqueue(&running_queue, &idle_tcb);

  uint8_t CMD_buffer[CMD_BUF_SIZE] = {0};
  cmd_buffer = CMD_buffer;

  global_flags.should_continue = 1;
  //read_buffer empty -> cannot read
  global_flags.can_read = 0;
  //write_buffer empty -> can write
  global_flags.can_write = 1;
  //avoids schedule starvation
  global_flags.check_read_first = 1;
  //Initially there's nothing to read
  global_flags.avr_read_lock = 1;
  //Initially the read_buffer is empty
  global_flags.avr_write_lock = 0;

  pc_read_pos = 0;
  avr_read_pos = 0;
  pc_write_pos = 0;
  avr_write_pos = 0;
  uint8_t ReadBuffer[BUF_SIZE] = {0};
  uint8_t WriteBuffer[BUF_SIZE] = {0};
  read_buffer = ReadBuffer;
  write_buffer = WriteBuffer;
  UART_init();
  //printf("starting\n");
  UART_putString((uint8_t *)"Starting!\n");
  startSchedule();
}
