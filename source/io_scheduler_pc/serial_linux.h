#pragma once
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

  typedef struct WriteArgs{
      long periodicity;
      char * write_string;
      //write_lenght could be derived from write_string but we
      //ship it through the args since we have to calculate it
      //inside the main
      uint8_t write_lenght;
  }WriteArgs;

  typedef struct ReadArgs{
      long periodicity;
      uint8_t read_lenght;
  }ReadArgs;

  typedef enum {Read=0x0, Write=0x1} CmdCode;

  //Appends buf to logfile. Returns 0 on success.
  int log_s(char * buf);
  //Reads from serial until command of type code (or error) is received.
  //Every other message is printed or logged accordingly
  //Puts the command inside buf and if relevant returns the
  //approved msg size. Returns -1 on error.
  int get_cmd(char * buf, uint8_t size, CmdCode code);
  //Called by the main thread after setting should_stop and waiting for the threads to finish
  void close_all();
  //! returns the descriptor of a serial port
  int serial_open(const char* name);

  //! sets the attributes
  int serial_set_interface_attribs(int fd, int speed, int parity);
  
  //! puts the port in blocking/nonblocking mode
  void serial_set_blocking(int fd, int should_block);

  //Reads up to size-1 bytes until a 0, \n or \r is encountered.
  int read_line(int fd, char * buf, uint8_t size);

  //Write the contents of the buffer up until size, 0, \r or \n
  //if size<0, its check will be skipped
  int write_buffer(int fd, char * buf, uint8_t size);

  int setup_serial(char * filename, int baudrate);

  //Exec write iteration: returns 0 on success, 1 if should_stop
  int exec_write_iteration(char * write_string, char * write_command, uint8_t write_command_lenght, char * reply);

  //Exec read iteration: returns 0 on success, 1 if should_stop
  int exec_read_iteration(char * read_command, uint8_t read_command_lenght, char * reply);

#ifdef __cplusplus
}
#endif
