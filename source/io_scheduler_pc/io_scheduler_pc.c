#include "serial_linux.h"
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

GlobalVars global_vars;
sem_t  ser_sem;
pthread_t logger;
pthread_t reader;
pthread_t writer;

int main(int argc, char** argv) {

  #ifdef DEBUG
  puts("Debug is set. Check the makefile to disable it");
  #endif
  long res;
  if (argc<3) {
    printf("usage: %s <filename> <baudrate>", argv[0]);
    return 0;
  }
  char* filename=argv[1];
  int baudrate=atoi(argv[2]);

  global_vars.serial_sem = &ser_sem;
  res = sem_init(global_vars.serial_sem, 0, 0);
  if(res == -1){
    perror("SemInit Error:");
    exit(EXIT_FAILURE);
  }


  char input_buffer[AVR_BUF_SIZE];
  puts("Manual or Automatic? [m/A]");
  fgets(input_buffer, 8, stdin);
  if(*input_buffer == 'm' || *input_buffer == 'M'){
    global_vars.is_manual = 1;
  }
  else global_vars.is_manual = 0;
  if(global_vars.is_manual){
    global_vars.should_print = 0;
  }
  else{
    puts("Print communication? [Y/n]");
    fgets(input_buffer, 8, stdin);
    if(*input_buffer == 'n' || *input_buffer == 'N'){
      global_vars.should_print = 0;
    }
    else global_vars.should_print = 1;
  }
  puts("Log? [Y/n]");
  fgets(input_buffer, 8, stdin);
  if(*input_buffer == 'n' || *input_buffer == 'N'){
    global_vars.log_file = NULL;
  }
  else{
    global_vars.log_file = fopen("logfile.txt", "w+");
    if(global_vars.log_file == NULL){
      perror("fopen error:");
      exit(EXIT_FAILURE);
    }
  }
  global_vars.should_stop = 0;
  if(global_vars.should_print || global_vars.log_file){
    #ifdef DEBUG
    puts("Setting up logger/printer");
    #endif
    global_vars.logger = &logger;
    //NOTE: logger thread
    pthread_create(global_vars.logger, NULL, logger_routine, NULL);
  }
  if(global_vars.is_manual==0){
    long read_periodicity;
    long write_periodicity;
    puts("Type write periodicity (usec): (0 for default)");
    scanf("%ld", &write_periodicity);
    puts("Type read periodicity (usec): (0 for default)");
    scanf("%ld", &read_periodicity);
    if(read_periodicity<=0){
      read_periodicity = DEFAULT_READ_PERIODICITY;
    }
    if(write_periodicity<=0){
      write_periodicity = DEFAULT_WRITE_PERIODICITY;
    }
    printf("Type read_lenght (bytes): (max %d, 0 for default)\n", AVR_BUF_SIZE-1);
    uint8_t read_lenght;
    scanf("%hhu", &read_lenght);
    if(read_lenght == 0 || read_lenght >= AVR_BUF_SIZE){
      read_lenght = DEFAULT_READ_LENGHT;
    }
    //Gets rid of \n
    fgetc(stdin);
    printf("Type string to write: (max %d)\n", AVR_BUF_SIZE-1);
    fgets(input_buffer, AVR_BUF_SIZE, stdin);
    uint8_t write_lenght = strlen(input_buffer);
    char write_string[write_lenght+1+BUF_PADDING];
    strcpy(write_string, input_buffer);

    WriteArgs writer_args = {0};
    writer_args.write_lenght = write_lenght;
    //Should be fine since main's RDA will not vanish
    writer_args.write_string = write_string;
    writer_args.periodicity = write_periodicity;

    ReadArgs reader_args = {0};
    reader_args.read_lenght = read_lenght;
    reader_args.periodicity = read_periodicity;

    global_vars.reader = &reader;
    global_vars.writer = &writer;


    puts("Simulation is starting... Type anything to stop");
    global_vars.serial_fd = setup_serial(filename, baudrate);
    if(global_vars.serial_fd<=0){
      fputs("Serial setup error", stderr);
      exit(EXIT_FAILURE);
    }
    res = sem_post(global_vars.serial_sem);
    if(res==-1){
      perror("SemWait Error");
      exit(EXIT_FAILURE);
    }
    //NOTE: Reader and Writer
    if(pthread_create(global_vars.writer, NULL, writer_routine, &writer_args)){
      fputs("Error: writer not created", stderr);
      exit(EXIT_FAILURE);
    }
    if(pthread_create(global_vars.reader, NULL, reader_routine, &reader_args)){
      fputs("Error: reader not created", stderr);
      exit(EXIT_FAILURE);
    }
    fgetc(stdin);
    #ifdef DEBUG
    puts("Closing off");
    #endif
    close_all();
  }
  else{
    //Start
    global_vars.serial_fd = setup_serial(filename, baudrate);
    if(global_vars.serial_fd<=0){
      fputs("Serial setup error", stderr);
      exit(EXIT_FAILURE);
    }
    res = sem_post(global_vars.serial_sem);
    if(res==-1){
      perror("SemWait Error");
      exit(EXIT_FAILURE);
    }
    puts("Simulation started.");
    do{
      printf("Type 1 for writing, 2 for reading, 3 for exiting:\n\n");
      fgets(input_buffer, 8, stdin);
      char choice = *input_buffer;
      if(choice == '1'){
        //Gets rid of \n
        //fgetc(stdin);
        printf("Type string to write: (max %d)\n", AVR_BUF_SIZE-1);
        fgets(input_buffer, AVR_BUF_SIZE, stdin);
        uint8_t write_lenght = strlen(input_buffer);;
        char write_string[write_lenght+1+BUF_PADDING];
        strcpy(write_string, input_buffer);
        //w + max 3 numbers + \r + \0
        char write_command[6+BUF_PADDING];
        sprintf(write_command, "w%hhu\r", write_lenght);
        printf("You typed: %s (write_command : %s)\n Executing...", write_string, write_command);
        res = exec_write_iteration(write_string, write_command, strlen(write_command), input_buffer);
        #ifdef DEBUG
        if(res == 1){
          fputs("Warning: write_iteration returned 1 in manual mode\n", stderr);
        }
        #endif
      }
      else if(choice == '2'){
        printf("Type read_lenght (bytes): (max %d, 0 for default)\n", AVR_BUF_SIZE-1);
        uint8_t read_lenght;
        scanf("%hhu", &read_lenght);
        if(read_lenght == 0 || read_lenght >= AVR_BUF_SIZE){
          read_lenght = DEFAULT_READ_LENGHT;
        }
        char read_command[6+BUF_PADDING];
        sprintf(read_command, "r%hhu\r", read_lenght);
        res = exec_read_iteration(read_command, strlen(read_command), input_buffer);
      }
      else if(choice == '3'){
        break;
      }
      else continue;
    }while(!global_vars.should_stop);
    #ifdef DEBUG
    puts("Closing...");
    #endif
    close_all();
  }
  puts("Everything closed gracefully. Exiting");
  return EXIT_SUCCESS;
}

void * logger_routine(void * args){
  long res;
  char logger_buffer[AVR_BUF_SIZE];
  //Could be while(1)
  while(!global_vars.should_stop){
    res = sem_wait(global_vars.serial_sem);
    if(res==-1){
      perror("SemWait Error");
      exit(EXIT_FAILURE);
    }
    if(global_vars.should_stop){
      break;
    }
    //puts("SemAcquired (logger)");
    read_line(global_vars.serial_fd, logger_buffer, AVR_BUF_SIZE);
    if(global_vars.should_print){
      printf("%s", logger_buffer);
    }
    if(global_vars.log_file){
      log_s(logger_buffer);
    }
    res = sem_post(global_vars.serial_sem);
    if(res == -1){
      perror("SemPost Error:");
      exit(EXIT_FAILURE);
    }
    usleep(100);
  }
  #ifdef DEBUG
  puts("==== LOGGER EXITING ====");
  #endif
  res = sem_post(global_vars.serial_sem);
  if(res == -1){
  perror("SemPost Error:");
  exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

void * reader_routine(void * args){
  ReadArgs * r_args = (ReadArgs *)args;
  long periodicity = r_args->periodicity;
  uint8_t read_lenght = r_args->read_lenght;
  //r + max 3 numbers + \r + 0
  char read_command[6+BUF_PADDING];
  char reply[AVR_BUF_SIZE];
  long res;
  sprintf(read_command, "r%hhu\r", read_lenght);
  uint8_t read_command_lenght = strlen(read_command);
  printf("read_command : %s\n", read_command);
  sleep(INITIAL_SLEEP);
  while(!global_vars.should_stop){
    #ifdef DEBUG
    puts("Sleeping...");
    #endif
    usleep(periodicity);
    #ifdef DEBUG
    puts("Reader waiting sem");
    #endif
    res = exec_read_iteration(read_command, read_command_lenght, reply);
    if(res == 1){
      break;
    }
  }
  puts("==== READER EXITING ====");
  res = sem_post(global_vars.serial_sem);
  if(res == -1){
    perror("SemPost Error:");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

void * writer_routine(void * args){
  WriteArgs * w_args = (WriteArgs *)args;
  long periodicity = w_args->periodicity;
  uint8_t write_lenght = w_args->write_lenght;
  char * write_string = w_args->write_string;
  //w + max 3 numbers + \r + 0
  char write_command[6+BUF_PADDING];
  char reply[AVR_BUF_SIZE];
  long res;
  sprintf(write_command, "w%hhu\r", write_lenght);
  printf("write_command : %s\n", write_command);
  uint8_t write_command_lenght = strlen(write_command);
  sleep(INITIAL_SLEEP);
  while(!global_vars.should_stop){
    #ifdef DEBUG
    puts("Sleeping...");
    #endif
    usleep(periodicity);
    #ifdef DEBUG
    puts("Writer waiting sem");
    #endif
    res = exec_write_iteration(write_string, write_command, write_command_lenght, reply);
    if(res == 1){
      break;
    }
  }
  puts("==== WRITER EXITING ====");
  res = sem_post(global_vars.serial_sem);
  if(res == -1){
    perror("SemPost Error:");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

