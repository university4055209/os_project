#include "serial_linux.h"
#include "main.h"
#include <errno.h>
#include <stdint.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>

int setup_serial(char *filename, int baudrate){
    printf( "opening serial device [%s] ... ", filename);
    int fd=serial_open(filename);
    if (fd<=0) {
      printf ("Error\n");
      return 0;
    } else {
      printf ("Success\n");
    }
    printf( "setting baudrate [%d] ... ", baudrate);
    int attribs=serial_set_interface_attribs(fd, baudrate, 0);
    if (attribs) {
      printf("Error\n");
      return 0;
    }
    serial_set_blocking(fd, 1);
    return fd;
}

int serial_set_interface_attribs(int fd, int speed, int parity) {
  struct termios tty;
  memset (&tty, 0, sizeof tty);
  if (tcgetattr (fd, &tty) != 0) {
    printf ("error %d from tcgetattr", errno);
    return -1;
  }
  switch (speed){
  case 19200:
    speed=B19200;
    break;
  case 57600:
    speed=B57600;
    break;
  case 115200:
    speed=B115200;
    break;
  case 230400:
    speed=B230400;
    break;
  case 576000:
    speed=B576000;
    break;
  case 921600:
    speed=B921600;
    break;
  default:
    printf("cannot sed baudrate %d\n", speed);
    return -1;
  }
  cfsetospeed (&tty, speed);
  cfsetispeed (&tty, speed);
  cfmakeraw(&tty);
  // enable reading
  tty.c_cflag &= ~(PARENB | PARODD);               // shut off parity
  tty.c_cflag |= parity;
  tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;      // 8-bit chars

  if (tcsetattr (fd, TCSANOW, &tty) != 0) {
    printf ("error %d from tcsetattr", errno);
    return -1;
  }
  return 0;
}

void serial_set_blocking(int fd, int should_block) {
  struct termios tty;
  memset (&tty, 0, sizeof tty);
  if (tcgetattr (fd, &tty) != 0) {
      printf ("error %d from tggetattr", errno);
      return;
  }

  tty.c_cc[VMIN]  = should_block ? 1 : 0;
  tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

  if (tcsetattr (fd, TCSANOW, &tty) != 0)
    printf ("error %d setting term attributes", errno);
}

int serial_open(const char* name) {
  int fd = open (name, O_RDWR | O_NOCTTY | O_SYNC );
  if (fd < 0) {
    printf ("error %d opening serial, fd %d\n", errno, fd);
  }
  return fd;
}

//Reads up to size-1 bytes until a 0, \n or \r is encountered.
int read_line(int fd, char * buf, uint8_t size){
  int bytes_read = 0;
  char last_char;
  int res;
  do{
    res = read(fd, buf+bytes_read, 1);
    if(!res){
      break;
    }
    if(res == -1){
      perror("Read error:");
      exit(EXIT_FAILURE);
    }
    bytes_read += res;
    last_char = buf[bytes_read-1];
  }while(last_char && last_char != '\n' && last_char != '\r' && bytes_read+1 < size);
  buf[bytes_read] = 0;
  return bytes_read;
}

//Write the contents of the buffer up until size, 0, \r or \n
//if size<0, its check will be skipped
int write_buffer(int fd, char * buf, uint8_t size){
  int bytes_written = 0;
  char last_char;
  int res;
  bool checksize = false;
  if(size>0){
    checksize = true;
  }
  do{
    res = write(fd, buf+bytes_written, 1);
    if(res==-1){
      perror("Write error:");
      exit(EXIT_FAILURE);
    }
    else if(res == 0){
      continue;
    }
    bytes_written += res;
    last_char = buf[bytes_written-1];
    #ifdef DEBUG
    printf("Sent %c\n", last_char);
    #endif
    if(checksize && bytes_written == size){
      break;
    }
  }while(last_char && last_char != '\n' && last_char != '\r');
  //We don't need to write \0 since the avr does it
  return bytes_written;
}

int log_s(char * buf){
  FILE * logfile = global_vars.log_file;
  if(logfile == NULL){
    fputs("[log_s] Cannot log into a NULL file!", stderr);
    return -1;
  }
  if(fputs(buf, logfile)<=0){
    perror("Logging error:");
    return -1;
  }
  else return 0;
}

int get_cmd(char * buf, uint8_t size, CmdCode code){
  if(!buf){
    fputs("[get_cmd] Error: Invalid buffer", stderr);
    return -1;
  }
  bool found = false;
  int res = 0;
  do{
    //TODO: Error handling
    res = read_line(global_vars.serial_fd, buf, size);
    //I could make one big if but I'd like the code to be readable
    if(code == Read && *buf=='r' && buf[res-1]=='\r'){
      found = true;
      #ifdef DEBUG
      printf("\nString found : %s\n", buf);
      #endif
    }
    else if(code == Write && *buf == 'w' && buf[res-1]=='\r'){
      found = true;
      #ifdef DEBUG
      printf("\nString found : %s\n", buf);
      #endif
    }
    else if(*buf == 'e' && buf[res-1]=='\r'){
      if(*(buf+1)=='d'){
	fputs("AVR_ERROR: The command sent was not understood by the avr.", stderr);
	return -1;
      }
      else if(*(buf+1)=='n'){
	fputs("AVR_ERROR: The command sent is not supported (yet) by the avr.", stderr);
	return -1;
      }
      else{
	//No variable errors are defined
      }
    } 
    else{
      if(global_vars.should_print){
        printf("%s", buf);
      }
      if(global_vars.log_file!=NULL){
        log_s(buf);
      }
    }
  }while(!found);
  //printf("\n\n msg found: %s \n\n", buf);
  res = atoi(buf+1);
  #ifdef DEBUG
  if(res == 0){
    fputs("[get_cmd] Warning: command returned a size of zero", stdout);
  }
  #endif
  return res;
}

//Takes care of sem_t and should_stop
void close_all(){
  long res;
  if(global_vars.serial_sem==NULL){
    fputs("[close_all] Error: semaphore is not valid.", stderr);
    exit(EXIT_FAILURE);
  }
  if(sem_wait(global_vars.serial_sem)==-1){
    perror("SemWait Error:");
    exit(EXIT_FAILURE);
  }
  global_vars.should_stop = 1;
  if(sem_post(global_vars.serial_sem)==-1){
    perror("SemPost Error:");
    exit(EXIT_FAILURE);
  }
  if(global_vars.reader){
    puts("Closing reader");
    res = pthread_join(*global_vars.reader, NULL);
    if(res){
      fputs("[close_all] Join error.", stderr);
      exit(EXIT_FAILURE);
    }
  }
  if(global_vars.writer){
    puts("Closing writer");
    res = pthread_join(*global_vars.writer, NULL);
    if(res){
      fputs("[close_all] Join error.", stderr);
      exit(EXIT_FAILURE);
    }
  }
  if(global_vars.logger){
    puts("Closing logger");
    res = pthread_join(*global_vars.logger, NULL);
    if(res){
      fputs("[close_all] Join error.", stderr);
      exit(EXIT_FAILURE);
    }
  }
  #ifdef DUBUG
  else{
    fputs("Warning: logger was not initiated!", stderr);
  }
  #endif
  puts("Closing sem");
  if(sem_destroy(global_vars.serial_sem)==-1){
    perror("SemDestroy Error:");
    exit(EXIT_FAILURE);
  }
  puts("Closing log_file");
  if(global_vars.log_file && fclose(global_vars.log_file)==EOF){
    perror("Cannot close logfile:");
    exit(EXIT_FAILURE);
  }
  puts("Closing serial");
  if(close(global_vars.serial_fd)==-1){
    perror("Cannot close serial:");
    exit(EXIT_FAILURE);
  }
  puts("Everything has been closed properly.");
}

//Exec write iteration: returns 0 on success, 1 if should_stop
int exec_write_iteration(char * write_string, char * write_command, uint8_t write_command_lenght, char * reply){
    long res;
    res = sem_wait(global_vars.serial_sem);
    if(res == -1){
      perror("SemWait Error");
      exit(EXIT_FAILURE);
    }
    if(global_vars.should_stop){
      return 1;
    }
    #ifdef DEBUG
    puts("SemAcquired (writer)");
    #endif
    write_buffer(global_vars.serial_fd, write_command, write_command_lenght);
    res = get_cmd(reply, AVR_BUF_SIZE, Write);
    if(res==-1){
      fputs("Error occurred. No write action taken", stderr);	
    }
    if(global_vars.should_print){
      printf("%s", reply);
    }
    if(global_vars.log_file){
      log_s(reply);
    }
    //We write our write_command up to the write limit. write_buffer does not write
    //any terminating string and that is good since we need to write EXACTLY the write_string
    write_buffer(global_vars.serial_fd, write_string, res);
    char temp = write_string[res];
    write_string[res] = 0;
    if(global_vars.should_print){
      printf("%s\n", write_string);
    }
    if(global_vars.log_file){
      log_s(write_string);
    }
    write_string[res] = temp;
    res = sem_post(global_vars.serial_sem);
    if(res == -1){
      perror("SemPost Error:");
      exit(EXIT_FAILURE);
    }
    return 0;
}

//Exec read iteration: returns 0 on success, 1 if should_stop
int exec_read_iteration(char * read_command, uint8_t read_command_lenght, char * reply){
    long res;
    res = sem_wait(global_vars.serial_sem);
    if(res == -1){
      perror("SemWait Error");
      exit(EXIT_FAILURE);
    }
    if(global_vars.should_stop){
      return 1;
    }
    #ifdef DEBUG
    puts("SemAcquired (reader)");
    #endif
    write_buffer(global_vars.serial_fd, read_command, read_command_lenght);
    res = get_cmd(reply, AVR_BUF_SIZE, Read);
    if(res==-1){
      fputs("Error occurred. No read action taken", stderr);	
    }
    if(global_vars.should_print){
      printf("%s", reply);
    }
    if(global_vars.log_file){
      log_s(reply);
    }
    //We should expect exactly res bytes but we put 1 byte extra for \0
    read_line(global_vars.serial_fd, reply, res+1);
    if(global_vars.should_print){
      printf("%s\n", reply);
    }
    if(global_vars.log_file){
      log_s(reply);
    }
    res = sem_post(global_vars.serial_sem);
    if(res == -1){
      perror("SemPost Error:");
      exit(EXIT_FAILURE);
    }
    return 0;
}
