#pragma once
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h>
#include <pthread.h>

typedef struct GlobalVars{
    int serial_fd;
    FILE * log_file;
    uint8_t should_print:1;
    uint8_t should_stop:1;
    uint8_t is_manual:1;
    sem_t *serial_sem;
    pthread_t *logger;
    pthread_t *reader;
    pthread_t *writer;
}GlobalVars;

extern GlobalVars global_vars;

void * logger_routine(void *);
void * writer_routine(void *);
void * reader_routine(void *);

#define BUF_MAX_SIZE 256
#define AVR_BUF_SIZE 128
#define DEFAULT_READ_PERIODICITY 2220500
#define DEFAULT_WRITE_PERIODICITY 3040025
#define DEFAULT_READ_LENGHT 30
#define INITIAL_SLEEP 3
#define BUF_PADDING 4
