# Asynchronous IO in Arduino Scheduler

Progetto per uno scheduler asincrono arduino con funzioni di scrittura e lettura da due buffer circolari. 

## Struttura e funzionamento 

Il progetto è suddivisibile nella parte arduino/avr e in quella pc. I file sorgente sono contenuti entrambi nella cartella `source`, rispettivamente in `io_scheduler_avr` e `io_scheduler_pc`. Associato allo sviluppo sono stati creati dei file di documentazione in `docs/` che illustrano la progettazione della parti salienti del progetto, tra cui il protocollo, i buffer circolari a doppio indice, lo scheduler modificato e la comunicazione con l'avr da parte del pc.

La parte arduino è consistita nella modifica del codice visto a lezione per la *uart* e per il *context switching su arduino*. Nello specifico, si è realizzato nel file `uart.c` l'implementazione di funzioni accessorie a quelle già disponibili per interagire con il pc seguendo il protocollo di cui `protocol.md` tra cui una `ISR` sulla ricezione di nuovi dati dalla seriale, delle funzioni per scrivere e ricevere stringhe di una specifica lunghezza e un altra per gestire i messaggi in arrivo (`handle_command`). Come poi descritto in `modifica_scheduler.md`, lo scheduler visto a lezione è stato modificato con l'aggiunta di funzioni che gestissero le nuove code di waiting. 

Il codice per il pc è stato realizzato partendo da quanto scritto sul file omonimo di documentazione `pc.md`. In sintesi sono state definite due modalità di interazione: 
- Una modalità manuale per il testing di tipologie di comandi diversi;
- Una modalità automatica adatta per verificare il comportamento generale del sistema. 

Per testare il progetto è sufficiente compilare il sorgente per l'arduino e inserirlo nella scheda tramite `(sudo) make main.hex` mentre il codice per il pc può essere costruito col comando `make` ed eseguito poi con `(sudo) ./io_scheduler_pc /dev/<file_seriale> 19200`.
