# Modifiche al context switching per l'aggiunta dell'io asincrono: 

Per rendere possibile una comunicazione asincrona occorre modificare lo scheduler corrente in modo da: 
1. Permettere ai processi di essere in *waiting*; 
2. Permettere ai processi di "buttarsi fuori" da soli dalla cpu quando non riescono a leggere/scrivere; 
3. Permettere alla funzione di scheduling corrente di verificare se ci siano processi da risvegliare in wait.

Tutto questo si può ottenere con le seguenti modifiche: 
1. Aggiunta dello status di `WaitingRead` e `WaitingWrite` per indicare dei processi in attesa per la lettura o la scrittura (`tcb.h`)
2. Aggiunta di due code per i nuovi status, rispettivamente `waiting_read_queue` e `waiting_write_queue` (`scheduler.h` + definizione su `scheduler.c`)
3. Creazione di una funzione di *deschedule* chiamata `deschedule`, atomica, da chiamare quando non si può leggere o scrivere (`scheduler.h` e `scheduler.c`)
4. Modifica della funzione di `schedule()` con un check sulla coda di read e quella di wait (`scheduler.h` e `scheduler.c`) con l'aggiunta di flag globali per evitare starvation su una delle due code (quella controllata per ultima)

Dal momento che i thread considerati inseriranno un carattere per volta - e anche per evitare problemi di starvation - possiamo semplificare il problema rimuovendo la difficoltà aggiunta di mantenere la priorità sull'io (i.e. ogni thread avrà come task la scrittura di *un solo carattere per volta* in modo da non doverci preoccupare di mantenere l'ordine di scrittura se ad esempio p1 intende scrivere "Hello" e p2 "World"). Per fare ciò dovremmo reinserire in testa alla waiting list il thread finché non termina la stringa da scrivere o alternativamente dovremmo concedere ai thread la possibilità di scrivere/leggere l'intera stringa prima di poter essere rimossi dalla cpu. 
