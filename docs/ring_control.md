# Funzionamento Ring Buffer

*Obiettivo*: Fare in modo che il PC legga i caratteri nell'ordine in cui sono stati scritti, per cui se in quest'ordine viene scritto `Hello World!` il Pc dovrà leggere `Hello World!` e fermarsi. Per farlo dobbiamo usare per ogni buffer due indici diversi, definire le condizioni iniziali e quelle di buffer pieno.

## Buffer di lettura (per il PC)

Il buffer di lettura per il PC corrisponde a quello di lettura per l'AVR. Definiamo:
+ `pc_read_pos` come la variabile che viene incrementata ad ogni accesso del pc al buffer; 
+ `avr_write_pos` come la variabile di scrittura, anch'essa incrementata ad ogni accesso al buffer da parte dell'avr; 
+ `pc_read_index = pc_read_pos % BUF_SIZE` che corrisponde all'indice nel buffer dell'*prossimo carattere da leggere*.
+ `avr_write_index = avr_write_pos % BUF_SIZE` che corrisponde all'indice nel buffer del *prossimo carattere da scrivere*. 

Si noti che per garantire l'uso completo del buffer il numero massimo rappresentabile dalle variabili deve essere ovviamente sufficientemente grande. Se `BUF_SIZE>256` e si è costretti ad usare una variabile a 16 bit allora il valore massimo per il buffer diventa `BUF_MAX_SIZE=65536=2^16` 

Se `avr_write_index == pc_read_index` possono esserci `BUF_SIZE` o `0` caratteri da leggere; possiamo distinguere i due casi grazie ad una flag. Negli altri casi il numero di caratteri disponibili dipenderà dal valore relativo che assumuno i due indici:
- Se `pc_read_index > avr_write_index` allora il numero di caratteri disponibili alla lettura sarà `BUF_SIZE-pc_read_index+avr_write_index`, vale a dire i caratteri che separano l'indice del pc alla fine del buffer più quelli che troverebbe ripartendo dall'inizio fino all'indice dell'avr;
- Se `pc_read_index < avr_write_index` allora il numero di caratteri sarà `avr_write_index-pc_read_index` essendo la distanza tra i due indici. 

In modo equivalente troviamo le formule per il numero di caratteri che l'avr può scrivere. Ovviamente, se `avr_write_index == pc_read_index` l'avr ha `BUF_SIZE` byte disponibili. Negli altri casi abbiamo:
- `pc_read_index-avr_write_index` se `pc_read_index > avr_write_index`;
- `BUF_SIZE-avr_write_index+pc_read_index` se `pc_read_index < avr_write_index`;

Si noti che sommando i caratteri da leggere o scrivere nei due casi si ottiene `BUF_SIZE`, ovvero il numero totale di caratteri. 

## Buffer di scrittura (per il PC)

In modo analogo a quello di scrittura definiamo: 
+ `pc_write_pos`;
+ `avr_read_pos`;
+ `pc_write_index = pc_write_pos % BUF_SIZE` *prossimo carattere da scrivere*;
+ `avr_read_index = avr_read_pos % BUF_SIZE` *prossimo carattere da leggere*;

Dall'analisi fatta per quello di lettura deriva:
+ Se `avr_read_index == pc_write_index` il pc può scrivere `BUF_SIZE` o `0` bytes e l'avr ne può leggere zero o `BUF_SIZE` a seconda della flag globale per il buffer;
+ Se `avr_read_index > pc_write_index` il pc può scrivere `avr_read_index-pc_write_index` bytes e l'avr può leggerene `BUF_SIZE-avr_read_index+pc_write_index`;
+ Se `avr_read_index < pc_write_index` il pc può scrivere `BUF_SIZE-pc_write_index+avr_read_index` bytes e l'avr può leggerne `pc_write_index-avr_read_index`;



