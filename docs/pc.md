# Struttura programma pc

Alla base del programma per il pc ci sono due elementi fondamentali:
- Un thread di logging il cui compito è quello di salvare su un file o stampare a schermo tutte le informazioni che non siano di controllo. Tali informazioni corrispondono a livello pratico a tutti quei messaggi non di controllo che l'avr invia tramite `putString`. 
- Un thread di controllo che si occupa di gestire le richieste dell'utente e di trasformarle in messaggi da inviare all'avr. 

## Funzionamento

Nella **fase iniziale** il pc richiede all'utente la scelta di due diverse modalità di interazione con l'avr: automatica e manuale. Per l'*interazione manuale* i messaggi scambiati con l'avr sono registrati in un file di log e viene offerta la possibilità all'utente di intervenire in qualsiasi momento richiedendo una lettura o una scrittura dei buffer. Queste richieste vengono salvate nel file di log e stampate a schermo per poi tornare alla schermata di scelta iniziale. Nel caso dell'*interazione automatica* viene definita nella fase iniziale una stringa di scrittura, la periodicità di lettura e quella di scrittura. Dopo aver salvato i parametri il pc stamperà a schermo i messaggi ottenuti ed effettuerà ad ogni periodicità delle operazioni di scrittura e/o di lettura. 

La **fase di comunicazione** è diversa a seconda dell'interazione scelta. Nel caso di quella manuale avremo il solo thread di logging e quello principale che si occuperanno della comunicazione e della gestione della seriale tramite un semaforo/mutex che garantisce la corretta gestione. Nel menù sarà scegliere di interrompere la comunicazione e questo porterà alla chiusura della seriale. 

Per l'interazione automatica definiremo due thread distinti che ad ogni periodicità tenteranno di acquisire il mutex per eseguire l'operazione precedentemente definita. Al thread principale sarà lasciato il compito di attendere l'eventuale inserimento di un segnale da parte dell'utente che indichi la chiusura della seriale. 

La **fase di chiusura** è sostanzialmente identica per entrambe le modalità. Associato al mutex sulla seriale avremo anche una variabile `should_stop` che sarà necessaria per stabilire quando interrompere le operazioni sulla seriale. Ogni volta che i thread secondari tenteranno di accedere alla seriale verificheranno `if(should_stop)` e in caso positivo interromperanno la comunicazione qualsiasi sia il loro stato. 

## Funzioni e Variabili 

A prescindere dalla modalità utilizzata definiamo delle funzioni e struct di utility fondamentali:

```c

//Read: rx\r, Write:wx\r where x in N and x < BUF_SIZE
typedef enum {Read=0x0, Write=0x1} CmdCode;

//Appends buf to logfile. Returns 0 on success.
int log_s(char * buf, FILE * logfile);
//Reads from serial until command of type code (or error) is received. 
//Every other message is printed or logged accordingly
//Puts the command inside buf and if relevant returns the 
//approved msg size. Returns -1 on error.
int get_cmd(char * buf, CmdCode code);
//Called by the main thread after setting should_stop and waiting for the threads to finish
void close_all();

sem_t * serial_sem;
pthread_t * logger;
```

Quando un thread cerca di interagire con la seriale per mandare un messaggio per prima cosa verrà richiesto l'accesso tramite semaforo, poi verrà verificata la possibilità di procedere tramite `should_stop`. In caso affermativo, si invierà il messaggio in questione per poi consumare tutti gli eventuali messaggi non di controllo ricevuti nel mentre. Ricevuta la conferma dal parte della seriale si proseguirà leggendo/scrivendo a seconda del caso. 

### Modalità Automatica

Per la modalità automatica definiamo le seguenti variabili, dai nome sufficientemente autodescrittivi: 

```c
typedef struct WriteArgs{
    long periodicity;
    char * write_string;
    //write_lenght could be derived from write_string but we
    //ship it through the args since we have to calculate it 
    //inside the main
    uint8_t write_lenght;
}WriteArgs;

typedef struct ReadArgs{
    long periodicity;
    uint8_t read_lenght;
}ReadArgs;

pthread_t * reader;
pthread_t * writer;

long read_periodicity; long write_periodicity;
uint8_t read_lenght; //BUF_SIZE < 256
char * write_string;
uint8_t write_lenght = strlen(write_string); //Ibidem
//Since we send \r write_lenght < BUF_SIZE
char * write_cmd = sprintf(...);
char * read_cmd = sprintf(...);
```

Il comportamento atteso è quello descritto nelle sezioni precedenti con un thread di lettura e uno di scrittura.

### Modalità Manuale

A differenza della modalità automatica non abbiamo bisogno di definire la periodicità ma restano fondamentali le indicazioni sulla lunghezza della stringa da scrivere e da leggere. Al posto dei thread dedicati a lettura e scrittura avremo il thread principale come esecutore. 





