# Protocollo comunicazione PC/AVR

Per migliorare l'efficienza della comunicazione ho scelto di utilizzare un protocollo che minimizzasse il numero di caratteri di controllo da scambiare tra PC e AVR, pur mantenendo la possibilità di aumentare in futuro il numero di comandi senza la necessità di stravolgere l'intera struttura del codice e del protocollo. Essendo attiva l'interruzione sulla ricezione, il funzionamento del protocollo è il seguente: 

1. Il PC invia un comando qualunque. Questo comando deve iniziare con una lettera identificatrice e terminare con `\r`.
2. L'AVR scrive l'intero comando in un piccolo buffer di controllo e chiama `UART_handle_command`. La funzione di controllo analizza il messaggio inviato e si occupa allo stesso modo della sua gestione. 

l'ISR che si occupa di gestire la comunicazione con la UART non ha anche il compito di risvegliare eventuali processi in waiting. Quello accadrà alla prossima interruzione per il timer o quando un thread tenterà di scrivere/leggere su un buffer pieno/vuoto.

```
                      ┌─┐                   ┌─┐                        
                      ║"│                   ║"│                        
                      └┬┘                   └┬┘                        
                      ┌┼┐                   ┌┼┐                        
                       │                     │                         
                      ┌┴┐                   ┌┴┐                        
                      PC                    AVR                        
                       │         GO          │                         
                       │────────────────────>│                         
                       │                     │                         
                       │                     │────┐                    
                       │                     │    │ Setup              
                       │                     │<───┘                    
                       │                     │                         
                       │       OK/ERR        │                         
                       │<─ ─ ─ ─ ─ ─ ─ ─ ─ ─ │                         
                       │                     │                         
                       │  READ(num_chars)    │                         
                       │────────────────────>│                         
                       │                     │                         
                       │                     │────┐                    
                       │                     │    │ Checks availability
                       │                     │<───┘                    
                       │                     │                         
  ╔═══════════════════╗│READ(num_available)  │                         
  ║0 -> empty buffer ░║│<─ ─ ─ ─ ─ ─ ─ ─ ─ ─ │                         
  ╚═══════════════════╝│                     │                         
                       │   (Termios) READ    │                         
                       │────────────────────>│                         
                       │                     │                         
                       │  WRITE(num_chars)   │                         
                       │────────────────────>│                         
                       │                     │                         
                       │                     │────┐                    
                       │                     │    │ Checks availability
                       │                     │<───┘                    
                       │                     │                         
  ╔══════════════════╗ │WRITE(num_available) │                         
  ║0 -> full buffer ░║ │<─ ─ ─ ─ ─ ─ ─ ─ ─ ─ │                         
  ╚══════════════════╝ │                     │                         
                       │  (Termios) WRITE    │                         
                       │────────────────────>│                         
                       │                     │                         
                       │        STOP         │                         
                       │────────────────────>│                         
                       │                     │                         
                       │                     │────┐                    
                       │                     │    │ Cleanup            
                       │                     │<───┘                    
                       │                     │                         
                       │       OK/ERR        │                         
                       │<─ ─ ─ ─ ─ ─ ─ ─ ─ ─ │                         
                      PC                    AVR                        
                      ┌─┐                   ┌─┐                        
                      ║"│                   ║"│                        
                      └┬┘                   └┬┘                        
                      ┌┼┐                   ┌┼┐                        
                       │                     │                         
                      ┌┴┐                   ┌┴┐                        
```

## Leggenda comandi protocollo

| Leggenda Comandi | Comando PC | Risposta AVR   |
|------------------|------------|----------------|
| Lettura          | `rx\r`     | `rx\r`         |
| Scrittura        | `wx\r`     | `wx\r`         |
| Start            | `g\r`      | `k\r` o `ex\r` |
| Stop             | `s\r`      | `k\r` o `ex\r` |

_Osservazione:_ I le `x` all'interno dei comandi rappresentano dei numeri e, per questo, possono essere variabili. Nel caso in cui ci sia un errore `ex\0` invierà un messaggio annesso di lunghezza `x`. I comandi di `start` e `stop` non sono più stati implementati dal momento che la loro funzione è svolta automaticamente dall'apertura/chiusura della seriale. Si veda `pc.md`. In più non sono stati implementati errori variabili al momento, bensì errori predefiniti caratterizzati dalla forma `e<error_code>\r`, in cui `<error_code>` rappresenta ad esempio una lettera che identifica univocamente un tipo di errore.
